def read_matrix(filename):
    """ loads a text file of a grid of integers and creates a list of lists
    of integers representing the matrix. matrix[r][c] is the element on
    row #r and column #c """
    with open(filename, 'r') as input_file:
        return [[int(column) for column in row.split()] for row in input_file]


def main():
    mat = read_matrix('testmatrix1.txt')
    numRows = len(mat)
    numCols = len(mat[0])
    rowSums = [0 for i in range(numRows)]
    colSums = [0 for i in range(numCols)]

    for i in range(numRows):
        for j in range(numCols):
            rowSums[i] += mat[i][j]

    for i in range(numCols):
        for j in range(numRows):

            colSums[i] += mat[j][i]

    sortedRows = list(enumerate(rowSums))
    sortedRows = sorted(sortedRows, key=lambda x: x[1])
    sortedCols = list(enumerate(colSums))
    sortedCols = sorted(sortedCols, key=lambda x: x[1])

    rowOrder = [x[0] for x in sortedRows]
    colOrder = [x[0] for x in sortedCols]

    # print(rowOrder)
    # print(colOrder)

    for i in range(numRows):
        print(mat[rowOrder[i]])

    print(' ')
    print(' ')

    for i in range(numRows):
        for j in range(numCols):
            print(mat[i][colOrder[j]]),
        print(' ')


if __name__ == '__main__':
    main()
