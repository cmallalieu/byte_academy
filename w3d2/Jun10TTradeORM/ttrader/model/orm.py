import sqlite3

class Sqlite3ORM:
    fields = []
    dbpath = ""
    dbtable = ""
    create = ""

    def __init__(self):
        raise NotImplementedError

    def save(self):
        if self.pk is None:
            self._insert()
        else:
            self._update()

    @classmethod
    def _create_insert(cls):
        columnlist = ", ".join(cls.fields)
        qmarks = ", ".join("?" for val in cls.fields)
        SQL = """ INSERT INTO {tablename} ({columnlist})
        VALUES ({qmarks}) """
        return SQL.format(tablename=cls.dbtable, columnlist=columnlist, qmarks=qmarks)

    def _insert(self):
        with sqlite3.connect(self.dbpath) as conn:
            cur = conn.cursor()
            SQL = self._create_insert()
            propvals = [getattr(self, propname) for propname in self.fields]

            cur.execute(SQL, propvals)
            self.pk = cur.lastrowid

    @classmethod
    def _create_update(cls):
        """ TODO: IMPLEMENT THIS. Return a generic UPDATE SQL command like 
        _create_insert did. You will want to be updating WHERE pk = ? """
        update_column_list = ", ".joinfield+"=?" for field in cls.fields)
        SQL = '''
              UPDATE {tablename} SET {update_column_list} WHERE pk = ?;
              '''
        return SQL.format(tablename=cls.dbtable, update_column_list=update_column_list)

    def _update(self):
        """ TODO: IMPLEMENT THIS. Execute the update statement. Remember that
        you will also have to provide self.pk for that ? in addition to the
        field values. """
        with sqlite3.connect(self.dbpath) as conn:
            cur = conn.cursor()
            SQL = self.create_update()
            propvals = [getattr(self, field) for field in self.fields + ["pk"]]
            cur.execute(SQL, propvals)


    def delete(self):
        SQL = f'''
                DELETE FROM {table_name} WHERE pk=?
              '''
        with connect(self.dbpath) as conn:
            cur = conn.cursor()
            cur.execute(SQL.format(tablename=self.dbtable), (self.pk))
            self.pk = None
