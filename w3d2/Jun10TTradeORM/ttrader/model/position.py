from model.orm import Sqlite3ORM

class Position(Sqlite3ORM):

    fields = ['ticker', 'amount', 'user_info_pk']
    dbpath = 'trader.db'
    dbtable = 'positions'

    create_sql = '''
                 CREATE TABLE positions
                 (
                 pk INTEGER PRIMARY KEY AUTOINCREMENT,
                 ticker VARCHAR(6) NOT NULL,
                 amount INTEGER,
                 user_info_pk INTEGER
                 FOREIGN KEY(user_info_pk) REFERENCES user_info(pk)
                 )
                 '''
