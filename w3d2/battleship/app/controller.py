from gameboard import GameBoard
from player import Player

p1 = Player()
p2 = Player()

def set_player(name, player_num):
    if player_num == 1:
        p1.set_name(name)
        return
    p2.set_name(name)

def place_ship(location, player_num):
    if p1.left_to_place > 0 and player_num == 1:
        if p1.board.get_space_value(location) == ' ':
            p1.board.place_ship_on_board(location, player_num)
            p1.left_to_place -= 1
        else:
            ask_location_again(player_num)
    elif p2.left_to_place > 0 and player_num == 2:
        if p2.board.get_space_value(location) == ' ':
            p2.board.place_ship_on_board(location, player_num)
            p2.left_to_place -= 1
        else:
            ask_location_again(player_num)
    else:
        return False
    return True

def ask_location_again(player_num):
    location = input('A ship is already placed there please pick again: ')
    parse_location(location)
    place_ship(location, player_num)

def parse_location(location):
    location_str = str(location)
    # print(f'location: {location_str}')
    x = int(location_str[0])
    y = int(location_str[1])
    return (x, y)

def ensure_valid_location(location, player_num):

    if not len(location) == 2:
        location = input(f'{get_player_name(player_num)}, enter a location to place a ship: ')
        ensure_valid_location(location, player_num)
    
    location = parse_location(location)

    if location[0] > 4 or location[0] < 0 or location[1] < 0 or location[1] > 4:
        print('must pick a valid location (00 - 44)')
        location = input(f'{get_player_name(player_num)}, enter a location to place a ship: ')
        ensure_valid_location(location, player_num)
    return location

def print_player_board(player_num):
    if player_num == 1:
        p1.board.printBoard()
    else:
        p2.board.printBoard()

def get_player_name(player_num):
    if player_num == 1:
        return p1.get_name()
    return p2.get_name()

def get_player_left_to_place(player_num):
    if player_num == 1:
        return p1.get_left_to_place()
    return p2.get_left_to_place()

def make_quess(location, player_num):
    if player_num == 1 and p2.board.guess(location): 
        return True
    elif player_num == 2 and p1.board.guess(location):
        return True
    return False 
