import controller as cont
import os
import time

def play_game():
    
	while ()

def setup_game():

    cont.set_player(input('Please enter the name of player 1: '), 1)
    cont.set_player(input('Please enter the name of player 2: '), 2)

    while True:
        tmp = cont.ensure_valid_location(input(f'{cont.get_player_name(1)}, enter a location to place a ship: '), 1)
        if cont.place_ship(tmp, 1) and cont.get_player_left_to_place(1):
            cont.print_player_board(1)
        else:
            cont.print_player_board(1)
            break

    time.sleep(3)
    os.system('clear')

    while True:
        tmp = cont.ensure_valid_location(input(f'{cont.get_player_name(2)}, enter a location to place a ship: '), 2)
        if cont.place_ship(tmp, 2) and cont.get_player_left_to_place(2):
            cont.print_player_board(2)
        else:
            cont.print_player_board(2)
            break

    time.sleep(3)
    os.system('clear')

if __name__ == '__main__':
    setup_game()
    play_game()