BLANK = ' '
SHIP = 'S'
MISS = 'M'
HIT = 'X'


class GameBoard:
    ship_locations= []
    ship_hits= [0,0,0]

    def __init__(self, width=5, height=5):
        grid = []
        for row_index in range(height):
            new_row = []
            for col_index in range(width):
                new_row.append(BLANK)
            grid.append(new_row)
        self.grid = grid

    def place_ship_on_board(self, location, player_num):
        x, y = location
        x = int(x)
        y = int(y)
        self.grid[x][y] = SHIP
        if self.grid[x][y] == ' ':
            self.grid[x][y] = SHIP
            ship_locations.append((x,y))
        else:
            return False

    def get_space_value(self, location):
        x, y = location
        x = int(x)
        y = int(y)
        # print(x)
        # print(y)
        value = self.grid[x][y]
        return value

    def printBoard(self):
        for row in self.grid:
            print(row)

    def guess(self, location):
        x, y = location
        x = int(x)
        y = int(y)
        if (x, y) for l in ship_locations:
            self.grid[x][y]= HIT
            ship_hits[ship_locations.index((x,y))]= 1
            return True
        else:
            self.grid[x][y] = MISS
            return False
