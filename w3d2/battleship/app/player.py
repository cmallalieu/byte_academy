from gameboard import GameBoard

class Player():

	def __init__(self):
		self.left_to_place = 3
		self.spaces_lost = []
		self.board = GameBoard()

	def set_name(self, name):
		self.name = name

	def get_left_to_place(self):
		return self.left_to_place

	def get_spaces_lost(self):
		return self.spaces_lost

	def get_name(self):
		return self.name
