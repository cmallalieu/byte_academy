import sqlite3
import csv

class Database:
    def __intit__(self):
        self.conn = sqlite3.connect('quiz2.db')
        self.cur = self.conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        if self.conn:
            if self.cur:
                self.conn.commit()
                self.cur.close()
        self.conn.close()

    def add_data(self, table_name, col_name, data):
        with sqlite3.connect('quiz2.db') as conn:
            cur = conn.cursor()
            SQL = '''
                  INSERT INTO table_name (col_name, data)
                  VALUES (?, ?, ?)
                  '''
            cur.execute(SQL, (table_name, col_name, data))
            self.pk = cur.lastrowid


if __name__ == '__main__':
    with Database() as db:
        with open('NVDA.csv') as nvda:
            reader = csv.reader(nvda)
            for line in reader:
                db.add_data('nvda', 'date', line[0])
                db.add_data('nvda', 'open', line[1])
                db.add_data('nvda', 'high', line[2])
                db.add_data('nvda', 'low', line[3])
                db.add_data('nvda', 'close', line[4])
                db.add_data('nvda', 'adj close', line[5])
                db.add_data('nvda', 'volume', line[6])

        with open('AMD.csv') as amd:
            reader = csv.reader(amd)
            for line in reader:
                db.add_data('amd', 'date', line[0])
                db.add_data('amd', 'open', line[1])
                db.add_data('amd', 'high', line[2])
                db.add_data('amd', 'low', line[3])
                db.add_data('amd', 'close', line[4])
                db.add_data('amd', 'adj close', line[5])
                db.add_data('amd', 'volume', line[6])
