import sqlite3

class Schema:

    def __init__(self):
        self.conn = sqlite3.connect('quiz2.db')
        self.cur = self.conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        if self.conn:
            if self.cur:
                self.conn.commit()
                self.cur.close()
            self.conn.close()

    def create_table(self, table_name):
        self.cur.execute(
            f'''
            DROP TABLE IF EXISTS {table_name}
            '''
        )

        self.cur.execute(
            f'''
            CREATE TABLE {table_name}
            (
            pk INTEGER PRIMARY KEY AUTOINCREMENT
            );
            '''
        )

    def modify_table(self, table_name, column_name, column_type):
        self.cur.execute(
            f'''
            ALTER TABLE {table_name}
            ADD COLUMN {column_name} {column_type};
            '''
        )

if __name__ == '__main__':
    schema = Schema()
    ## create table for NVDA
    schema.create_table('NVDA')
    schema.modify_table('NVDA', 'date', 'VARCHAR')
    schema.modify_table('NVDA', 'open', 'FLOAT')
    schema.modify_table('NVDA', 'high', 'FLOAT')
    schema.modify_table('NVDA', 'low', 'FLOAT')
    schema.modify_table('NVDA', 'close', 'FLOAT')
    schema.modify_table('NVDA', 'adj close', 'FLOAT')
    schema.modify_table('NVDA', 'volume', 'INT')

    ## Create table for AMD
    schema.create_table('AMD')
    schema.modify_table('AMD', 'date', 'VARCHAR')
    schema.modify_table('AMD', 'open', 'FLOAT')
    schema.modify_table('AMD', 'high', 'FLOAT')
    schema.modify_table('AMD', 'low', 'FLOAT')
    schema.modify_table('AMD', 'close', 'FLOAT')
    schema.modify_table('AMD', 'adj close', 'FLOAT')
    schema.modify_table('AMD', 'voluma', 'INT')
    
