#!/usr/bin/env python


def main_menu():
    print('Welcome to Terminal Teller!:\n')
    print('1) Create Account')
    print('2) Log In')
    print('3) Quit')
    return input('\nWhat do you want to do? ')


if __name__ == '__main__':
    print(main_menu())