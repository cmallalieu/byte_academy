import os


def main_menu():
    print('#' * 40)
    print('\n[l] Look-up\n[q] Quote\n[e] Exit')
    return input('\nWhat do you want to do?')


def lookup_menu():
    return input('Company name: ')


def quote_menu():
    return input('Ticker Symbol: ')


if __name__ == '__main__':
    print(main_menu())