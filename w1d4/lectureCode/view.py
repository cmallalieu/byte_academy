#!/usr/bin/env python3

import os
import time


def main_menu():
    #print('#'*40)
    os.system('cowsay -d "Welcome to my CLI app. - $(whoami)"')
    time.sleep(2)
    os.system('clear')
    print('\n[l] Look-up\n[q] Quote\n[e] Exit')
    return input('\nWhat do you want to do? ')


def lookup_menu():
    return input('Company Name: ')


def quote_menu():
    return input('Ticker Symbol: ')


if __name__ == '__main__':
    print(main_menu())
