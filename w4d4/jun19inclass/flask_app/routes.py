from flask import render_template, request, jsonify, session, redirect, url_for
from .app import app

@app.route("/", methods=["GET"])
def root():

    return render_template("index.html", name= "Mike Bloom")

@app.route("/login", methods=["POST"])
def login():
    session["username"]= request.form["username"]
    return redirect(url_for("landing"))

@app.route("/landing",methods=["GET"])
def landing():
    username= session.get("username")
    return render_template("landing.html", username = username)