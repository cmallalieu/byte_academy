from flask import Flask

app = Flask(__name__)
app.secret_key = "secrets"

from . import routes
