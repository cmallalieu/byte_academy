import sqlite3


class Database:
    def __init__(self):
        self.connection = sqlite3.connect('employees.db',
                                          check_same_thread=False)
        self.cursor = self.connection.cursor()

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        if self.connection:
            if self.cursor:
                self.connection.commit()
                self.cursor.close()
            self.connection.close()

    def create_table(self, table_name):
        self.cursor.execute(f'DROP TABLE IF EXISTS {table_name};')
        self.cursor.execute(f'''CREATE TABLE {table_name}(
                pk INTEGER PRIMARY KEY AUTOINCREMENT
            );''')

    def add_column(self, table_name, column_name, column_type):
        self.cursor.execute(f'''ALTER TABLE {table_name}
			ADD COLUMN {column_name} {column_type};
			''')


if __name__ == '__main__':
    user_framework = {
        'table_name':
        'user',
        'columns': [{
            'column_name': 'name',
            'column_type': 'VARCHAR'
        }, {
            'column_name': 'email',
            'column_type': 'VARCHAR'
        }, {
            'column_name': 'country',
            'column_type': 'VARCHAR'
        }]
    }
    phone_number_framework = {
        'table_name':
        'phone_number',
        'columns': [{
            'column_name': 'cellphone',
            'column_type': 'VARCHAR'
        }, {
            'column_name': 'homephone',
            'column_type': 'VARCHAR'
        }, {
            'column_name': 'workphone',
            'column_type': 'VARCHAR'
        }]
    }

    with Database() as db:
        db.create_table(user_framework['table_name'])
        for _ in user_framework['columns']:
            db.add_column(user_framework['table_name'], _['column_name'],
                          _['column_type'])

        db.create_table(phone_number_framework['table_name'])
        for _ in phone_number_framework['columns']:
            db.add_column(phone_number_framework['table_name'], _['column_name'],
                          _['column_type'])
