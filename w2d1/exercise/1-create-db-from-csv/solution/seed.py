import csv
import sqlite3

def table_maker():

	

	names = []
	cellphones = []
	homephones = []
	workphones = []
	emails = []
	countries = []


	with open('../employees.csv') as f:
		reader = csv.reader(f)
		for row in reader:
			names.append(row[0])
			cellphones.append(row[1])
			homephones.append(row[2])
			workphones.append(row[3])
			emails.append(row[4])
			countries.append(row[5])

	numNames = len(names)

	for i in range(numNames):
		cursor.execute(
			f'''
			INSERT INTO user(
					name,
					email,
					country
			) VALUES (
					'{names[i]}',
					'{emails[i]}',
					'{countries[i]}'
			);
			'''
		)
		
		cursor.execute(
			f'''
			INSERT INTO phone_number(
					cellphone,
					homephone,
					workphone
			) VALUES (
					'{cellphones[i]}',
					'{homephones[i]}',
					'{workphones[i]}'
			);
			'''
		)


if __name__ == '__main__':
	connection = sqlite3.connect('employees.db', check_same_thread=False)

	cursor = connection.cursor()

	table_maker()

	connection.commit()
	cursor.close()
	connection.close()



