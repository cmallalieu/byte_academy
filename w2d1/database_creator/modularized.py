import sqlite3

class Database:

	def __init__(self):
		self.connection= sqlite3.connect('test2.db', check_same_thread=False)
		self.cursor = self.connection.cursor()
		print('__init__')

	def __enter__(self):
		print('inside __enter__')
		return self

	def __exit__(self, type_, value, traceback):
		print('inside __exit__')
		# only try and close connection and cursor if on exists
		if self.connection:
			if self.cursor:
				self.connection.comit()
				self.cursor.close()
			self.connection.close()

	def create_table(self, table_name):
		self.cursor.execute(
			f'''CREATE TABLE {table_name}
			(
			pk INTEGER PRIMARY KEY AUTOINCREMENT)
			);'''
		)

	def alter_table(self, table_name, column_name, column_type):
		self.cursor.execute()
		


if __name__ == '__main__':
	print('inside if __name__ equals "__main__" thing')
	with Database() as db:
		db.create_table('uber')
