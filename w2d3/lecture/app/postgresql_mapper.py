#!/usr/bin/env python3

import sqlite3


class Database:

	def __init__(self):
		print('inside __init__','self:',self)
		self.connection = sqlite3.connect('test.db')
		self.cursor     = self.connection.cursor()

	def __enter__(self):
		print('inside __enter__','self:',self)
		return self

	def __exit__(self,type_,value,traceback):
		print('inside __exit__','self:',self)
		# TODO 
		# - Close a `self.connection` to the database 
		# - Close a `self.connection` to a `self.cursor`
		if self.connection:
			print('inside of self.connection')
			if self.cursor:
				print('inside of self.connection')
				# FIXME
				# - The changes to the database aren't being saved!
				self.connection.commit()	
				self.cursor.close()
			self.connection.close()
			# FIXME 

if __name__ == '__main__':
	with Database() as d:
		print('inside of the with block')
	print('outside of the with block')
