from model.user import User
from unittest import TestCase
from schema import build_user

class TestUser(TestCase):

    def setUp(self):
        build_user()

        mike = User(**
        {
            "username": "mikebloom",
            "password": "password",
            "realname": "Mike Bloom",
            "balance": "10000.0"
        })

        mike.save()

    def tearDown(self):
        pass


    def testFromPk(self):
        mike = User.frompk(1)
        self.assertEqual(mike.realname, "Mike Bloom", "Lookup from pk populates instance properties.")
