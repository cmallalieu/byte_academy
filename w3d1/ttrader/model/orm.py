import sqlite3

class Sqlite3ORM:
    fields = []
    dbpath = ""
    dbtable = ""
    create = ""

    def __init__(self):
        raise NotImplementedError

    @classmethod
    def _create_insert(cls):
        columnlist = ", ".join(cls.fields)
        qmarks = ", ".join("?" for val in cls.fields)
        SQL = '''
              INSERT INTO {table_name} ({column_list})
              VALUES ({qmarks})
              '''
        return SQL.format(tablename=cls.dbtable
