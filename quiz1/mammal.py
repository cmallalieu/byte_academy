from animal import Animal

class Mammal(Animal):

	def __init__(self):
		Animal.__init__(self)
		self.fur = True

if __name__ == '__main__':
	print(Mammal())
