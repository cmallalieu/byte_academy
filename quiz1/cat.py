from mammal import Mammal

class Cat(Mammal):

	def __init__(self):
		Mammal.__init__(self)
		self.meow = True


if __name__ == '__main__':
	print('Cat Pulse: %s') % (Cat().pulse)
	print('Cat Fur: %s') % (Cat().fur)
	print('Cat Meow: %s') % (Cat().meow)