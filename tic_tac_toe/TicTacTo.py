#TODO fix recursion in userInput function, value does not update if first attempt is invalid input

import sys

def createMap():
	line0 =  list("       |       |       ")
	line1 =  list("       |       |       ")
	line2 =  list("       |       |       ")
	line3 =  list("       |       |       ")
	line4 =  list("-------+-------+-------")
	line5 =  list("       |       |       ")
	line6 =  list("       |       |       ")
	line7 =  list("       |       |       ")
	line8 =  list("       |       |       ")
	line9 =  list("-------+-------+-------")
	line10 = list("       |       |       ")
	line11 = list("       |       |       ")
	line12 = list("       |       |       ")
	line13 = list("       |       |       ")   

	map = []
	map.append(line0)
	map.append(line1)
	map.append(line2)
	map.append(line3)
	map.append(line4)
	map.append(line5)
	map.append(line6)
	map.append(line7)
	map.append(line8)
	map.append(line9)
	map.append(line10)
	map.append(line11)
	map.append(line12)
	map.append(line13)

	return map

def printMap(map):

	for line in map:
		print("".join(line))

def createGame():
	game = [[0,0,0],[0,0,0],[0,0,0]]

	return game

def drawO(line1, line2, line3, line4, position, game):
	
	if position % 3 == 0:
		relPosition = 0
	elif position % 3 == 1:
		relPosition = 8
	elif position % 3 == 2:
		relPosition = 16

	line1[relPosition + 3] = "_"
	line2[relPosition + 2] = "/"
	line2[relPosition + 4] = "\\"
	line3[relPosition + 2] = "\\"
	line3[relPosition + 4] = "/"
	line4[relPosition + 3] = "-"

	updateGame(2, position, game)

def drawX(line1, line2, line3, line4, position, game):
	
	
	if position % 3 == 0:
		relPosition = 0
	elif position % 3 == 1:
		relPosition = 8
	elif position % 3 == 2:
		relPosition = 16

	line1[relPosition + 1] = "\\"
	line1[relPosition + 5] = "/"
	line2[relPosition + 2] = "\\"
	line2[relPosition + 4] = "/"
	line3[relPosition + 3] = "X"
	line4[relPosition + 4] = "\\"
	line4[relPosition + 2] = "/"

	updateGame(1, position, game)

def updateGame(playerNum, position, game):

	if playerNum == 1:
		game[position % 3][int(int(position) / 3)] = 1
	if playerNum == 2:
		game[position % 3][int(int(position) / 3)] = 2

def checkWin(game):

	if game[0][0] == game[1][0] == game[2][0] and game[0] != 0:
		return game[0][0]
	elif game[0][1] == game[1][1] == game[2][1] and game[0][1] != 0:
		return game[0][1]
	elif game[0][2] == game[1][2] == game[2][2] and game[0][2] != 0:
		return game[0][2]
	elif game[0][0] == game[0][1] == game[0][2] and game[0][0] != 0:
		return game[0][0]
	elif game[1][0] == game[1][1] == game[1][2] and game[1][0] != 0:
		return game[1][0]
	elif game[2][0] == game[2][1] == game[2][2] and game[2][0] != 0:
		return game[2][0]
	elif game[0][0] == game[1][1] == game[2][2] and game[0][0] != 0:
		return game[0][0]
	elif game[0][2] == game[1][1] == game[2][0] and game[2][2] != 0:
		return game[2][2]
	return 0;

def doesOverlapping(game, position):
	if game[position % 3][int(int(position) / 3)] != 0:
		return 1;
	else:
		return 0;

def userInput(game):
	
	move = input()
	if not move.isdigit():
		print("You Must Enter An Int")
		userInput(game)
	elif int(move) < 0 or int(move) > 8:
		print("Must Enter An Int Between 0 and 8")
		userInput(game)
	elif doesOverlapping(game, int(move)) == 1:
		print("Cannot Move On The Same Space, Choose Again")
		userInput(game)
	return int(move)	

def playAgain(game):
	check = input("Would You Like Play Again? Input y or n: ")
	if 	check == "y":
		main()
	elif check == "n":
		sys.exit()

def main():
	map = createMap()
	game = createGame()
	printMap(map)
	for i in range(0, 9):
		if checkWin(game) == 0:		
			if i % 2 == 0:
				print("Player 1's Move: ")
				move = userInput(game)
				#moveList = [userInput(game)]
				#move = moveList[-1]
				if move >= 0 and move < 3:
					drawX(map[0], map[1], map[2], map[3], move, game) 
				elif move >= 3 and move < 6:
					drawX(map[5], map[6], map[7], map[8], move, game)
				elif move >=6 and move < 9:
					drawX(map[10], map[11], map[12], map[13], move, game)
				printMap(map)
			if i % 2 == 1:
				print("Player 2's Move: ")
				move = userInput(game)
				#moveList = [userInput(game)]
				#move = moveList[-1]
				if move >= 0 and move < 3:
					drawO(map[0], map[1], map[2], map[3], move, game) 
				elif move >= 3 and move < 6:
					drawO(map[5], map[6], map[7], map[8], move, game)
				elif move >=6 and move < 9:
					drawO(map[10], map[11], map[12], map[13], move, game)
				printMap(map)
		elif checkWin(game) != 0:
			print("THE WINNER IS! Player", checkWin(game))
			playAgain(game)
	print("It is A Tie!")
	playAgain(game)

main()
