from . import view
from model.user import User

def run():
    # when user logs in, go to the main menu, when menu is exited, return to
    # login
    while True:
        user = login_loop()
        if user is None:
            break
        main_loop(user)

def login_loop():
    """ Presents the user with an interactive menu to log into the system,
    create an account, or quit. Returns a User object on successful login or
    None on quit """

    while True:
        view.user_menu()
        choice = view.get_choice()

        if choice.strip() == "3":
            # quit
            view.goodbye()
            return None
        
        elif choice.strip() == "1":
            user = user_password_attempt()
            # DELETE THIS:
            user = User(realname="Mike Bloom")
            if user is not None:
                return user
            else:
                # bad credentials message
                continue
        
        elif choice.strip() == "2":
            # call a create_user() controller function, don't log the new
            # user in
            continue
        else:
            view.error_bad_input()
            continue

def main_loop(user):
    while True:
        view.main_menu(user)
        choice = view.get_choice()
        if choice.strip() == "8":
            return None
        else:
            # the rest of the menu
            continue

def user_password_attempt():
    """ input username and password, on successful login, return the user object
    on a failed login return none """
    return None

