import sqlite3
from .orm import Sqlite3ORM
from .position import Position
from .trade import Trade
from .util import get_price
import bcrypt

#DBNAME="trader.db"
#TABLENAME="user_info"

# newguy = User(username="allegedly_gregory", realname="Greg Stannard")

class InsufficientFundsError(Exception):
    pass

class InsufficientSharesError(Exception):
    pass

class User(Sqlite3ORM):

    fields = ['username', 'password', 'realname', 'balance']
    dbtable = "user_info"
    dbpath = "trader.db"

    def __init__(self, **kwargs):
        self.pk = kwargs.get('pk')
        self.username = kwargs.get('username')
        self.password = kwargs.get('password')
        self.realname = kwargs.get('realname')
        self.balance = kwargs.get('balance', 0.0)

    def hash_password(self, password):
        """ someuser.hash_password("somepassword") sets someuser's self.password
        to a bcrypt encoded hash """
        self.password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

    @classmethod
    def login(cls, username, password):
        """ search for the user with the given username (use one_where) and then
        use bcrypt's checkpw() to verify that the credentials are correct
        return None for bad credentials or the matching User instance on a
        successful login """
        user = cls.one_where("username=?", (username,))
        if user is None:
            return None
        if bcrypt.checkpw(password.encode(), user.password):
            return user
        return None

    def all_positions(self):
        """ return all Positions for this user as a list """
        positions = Position.many_where("user_info_pk=?", (self.pk,))
        return positions

    def position_for_stock(self, ticker):
        """ return a user's position in one stock or None """
        position = Position.one_where("user_info_pk=? AND ticker=?", (self.pk, ticker))
        return position

    def buy(self, ticker, amount):
        # TODO: make a trade (volume=amount), price = price of 1 share
        """ buy a stock. if there is no current position, create one, if there is
        increase its amount. no return value """
        if amount < 0:
            raise ValueError
        ticker_price = get_price(ticker)
        cost = ticker_price * amount
        if self.balance < cost:
            raise InsufficientFundsError
        self.balance -= cost
        current_position = self.position_for_stock(ticker)
        if current_position is None:
            current_position = Position(ticker=ticker, amount=0, user_info_pk=self.pk)
        current_position.amount += amount
        current_position.save()
        new_trade = Trade(ticker=ticker, volume=amount, price=ticker_price, user_info_pk=self.pk)
        new_trade.save()
        self.save()

    def sell(self, ticker, amount):
        # TODO, make a trade (volume=-amount) price = price of 1 share
        if amount < 0:
            raise ValueError
        cost = get_price(ticker) * amount
        position = self.position_for_stock(ticker)
        if position is None or amount > position.amount:
            raise InsufficientSharesError
        position.amount -= amount
        self.balance += cost
        position.save()
        subtract_amount = -amount
        new_trade = Trade(ticker=ticker, volume=subtract_amount, price=cost, user_info_pk=self.pk)
        new_trade.save()

        self.save()

    def all_trades(self):
        """ return a list of Trade objects for every trade made by this user
        arranged oldest to newest """
        list = Trade.many_where("user_info_pk=? ORDER BY time", (self.pk,))
        return list

    def trades_for(self, ticker):
        """ return a list of Trade objects for each trade of a given stock for
        this user, arranged oldest to newest """

        list = Trade.many_where("user_info_pk=? AND ticker=? ORDER BY time", (self.pk, ticker))
        return list

if 0 == 1:
    try:
        user.buy(ticker, amount)
    except InsufficientFundsError:
        view.insufficient_funds()
