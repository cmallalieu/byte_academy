from model.user import User
from model.trade import Trade
from unittest import TestCase
from schema import build_user, build_positions, build_trades

# to execute from the base of the project directory structure run:
# python3 -m unittest tests/testTrade.py
# to run all tests in a directory run:
# python3 -m unittest tests discover

class TestTrade(TestCase):

    def setUp(self):
        build_user()
        build_positions()
        build_trades()


        mike = User(**{
            "username": "mikebloom",
            "realname": "Mike Bloom",
            "balance": 10000000.0
        })
        mike.hash_password("password")
        mike.save()


        mike.buy("stok", 2)
        mike.sell("stok", 1)
        #mike.buy("goog", 4)

    def tearDown(self):
        pass

    def testDummy(self):
        pass

    def testOneWhere(self):
        # student exercise
        pass

    def testSave(self):
        # student exercise
        pass

    def testAllTrades(self):
        mike = User.from_pk(1)
        history = mike.all_trades()
        print(history)
        self.assertIsInstance(history, list, "all_trades returns a list")
        firstHistory = history[0]
        self.assertIsInstance(firstHistory, Trade, "all_trades returns a trade object")

    def testTradesFor(self):
        mike = User.from_pk(1)
        history = mike.trades_for('stok')
        self.assertIsInstance(history, list, "trades_for returns a list")
        firstHistory = history[0]
        self.assertIsInstance(firstHistory, Trade, "trades_for returns a trade object")
