

class Fizz_Buzzer():

	def __init__(self, start=0):
		self.start = start
		self.number = self.start

	def next(self):
		self.number += 1
		if self.number % 3 == 0 and self.number % 5 == 0:
			return 'FizzBuzz'
		elif self.number % 3 == 0:
			return 'Fizz'
		elif self.number % 5 == 0:
			return 'Buzz'
		else:
			return str(self.number)


if __name__ == '__main__':
	buzzer = Fizz_Buzzer(11)
	print(buzzer.next())
	print(buzzer.next())
	print(buzzer.next())
	print(buzzer.next())
	print(buzzer.next())