from quiz3 import Fizz_Buzzer
from unittest import TestCase

class testFizzBuzz(TestCase):

	def testDefaultStart(self):
		x = Fizz_Buzzer()
		self.assertEqual(x.number, 0, 'Default starting value is incorrect')

	def testNext(self):
		x = Fizz_Buzzer()
		self.assertEqual(x.next(), '1', 'Next function does not work')

	def testCustomStart(self):
		x = Fizz_Buzzer(2)
		self.assertEqual(x.number, 2, 'Cannot set starting num other than 0')

	def testFizzNum(self):
		x = Fizz_Buzzer(2)
		self.assertEqual(x.next(), 'Fizz', '3 is not recognized as a Fizz')
		self.assertEqual(x.next(), '4', '4 is not recognized as a normal number')
