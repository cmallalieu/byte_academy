## Querying a SQL Database

In this challenge you are given a SQL database with data inside. You will need to mine it for important information.

Open it up in terminal by typing
```bash
$ sqlite3 sitemetrics.db
```
To see the existing tables and columns, use the .schema command. Map this by hand or on SQL designer so you have a greater understanding of what we're working with.

#### Answer the following questions in this file, with the results and the sql you wrote to get it.
-------------

##### How many people are from California?
SELECT state, COUNT(*) FROM users GROUP BY state;
CA|14


##### Who has the most page views? How many do they have, and where are they from?
SELECT users.name, users.page_views, user_searches.user_id FROM users INNER JOIN user_searches ON users.id=user_searches.user_id GROUP BY users.page_views, user_searches.user_id;
Edison Mcintyre|19937|179


##### Who has the least page views? How many do they have and where are they from?
SELECT users.name, users.page_views, user_searches.user_id FROM users INNER JOIN user_searches ON users.id=user_searches.user_id GROUP BY users.page_views, user_searches.user_id;
Hattie Ross|16|477


##### Who are the most recent visitors to the site?(at least 3)
SELECT name, last_visit FROM users ORDER BY last_visit DESC LIMIT 3;
Otha Ortiz|2014-10-08
Selina Hardy|2014-10-08
Terrance Allen|2014-10-08


##### Who was the first visitor?
SELECT name, last_visit FROM users ORDER BY last_visit ASC LIMIT 1;
Woodrow Duffy|2013-10-08


##### Who has an email address with the domain 'horse.edu'?
SELECT email FROM users WHERE email LIKE '%@horse.edu';
steve.louis.jeremy@horse.edu


##### How many people are from the city Graford?
SELECT city, COUNT(*) FROM users WHERE city IS 'Graford';
Graford|3


##### What are the names of all the cities that start with the letter V, in alphabetical order?
SELECT DISTINCT city FROM users WHERE city LIKE 'V%' ORDER BY city ASC;
Valley View
Van
Vega
Victoria


##### What are the names and home cities for people searched for the word "drain"?



##### How many times was "trousers" a search term?
SELECT word, COUNT(*) FROM search_terms WHERE word IS 'trousers';
trousers|1


##### What were the search terms used by visitors who last visited on August 22 2014?

##### What was the most frequently used search term by people from Idaho?

##### What is the name of user 391, and what are his search terms?
