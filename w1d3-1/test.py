def numPairs(numSocks, sockColors):
    numOfColors = {}
    numPairs = 0

    for c in set(sockColors):
        numOfColors[c] = 0

    for color in sockColors:
        numOfColors[color] += 1

    for numColors in numOfColors:
        numPairs += numOfColors[numColors] / 2

    return (numPairs)
