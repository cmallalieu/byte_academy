import csv

data = []
with open('NHL_2018.csv') as f:
    for line in f:
        line = line.rstrip().split(',')
        data.append(line)

    detPlayers = [x for x in data if x[3] == 'DET']
    res = []
    for player in detPlayers:
        newPlayer = []
        GP = int(player[5])
        ATOI = float(player[22][:1]) + (float(player[22][3:]) / 60)
        normalizer = (82 / GP) * (60 / ATOI)
        PTS = int(float(player[8]) * normalizer)
        BLK = int(float(player[23]) * normalizer)
        HIT = int(float(player[24]) * normalizer)
        name = player[1].split('\\')[0]
        newPlayer.append(name)
        newPlayer.append(GP)
        newPlayer.append(PTS)
        newPlayer.append(BLK)
        newPlayer.append(HIT)
        res.append(newPlayer)

    with open('new_csv.csv', "wb") as newFile:
        writer = csv.writer(newFile)
        writer.writerows(res)

    # numCols = len(res)
    # numRows = len(res[0])

    # for i in range(numCols):
    # 	for j in range(numRows):

    # for player in detPlayers:
    #     wholeName = player[1]
    #     name = wholeName.split('//')[0]
    #     print(name)
