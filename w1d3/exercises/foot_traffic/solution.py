'''
person - room # - In/Out - time(min)
0 5 I 330
1 15 I 1061
2 21 I 319
3 12 I 1067
4 35 I 989
5 20 I 704
'''
from copy import deepcopy


def common_entries(*dcts):
    for i in set(dcts[0]).intersection(*dcts[1:]):
        yield (i, ) + tuple(d[i] for d in dcts)


data = []
with open('traffic.txt', 'r') as f:
    for line in f:
        line = line.rstrip().split(' ')
        data.append(line)

lastRoomNum = 0
currentRoomNum = 0

rooms1 = {}

for entry in data:
    rooms1[entry[1]] = 0

rooms2 = deepcopy(rooms1)

for entry in data:
    if entry[2] == 'I':
        rooms1[entry[1]] += 1

for entry in data:
    if entry[2] == 'O':
        rooms2[entry[1]] += int(entry[3])
    else:
        rooms2[entry[1]] -= int(entry[3])

for room in rooms2:
    rooms2[room] /= int(rooms1[room])

res = (list(common_entries(rooms1, rooms2)))
numRooms = len(rooms1)
printList = [0 for i in range(numRooms)]

for i in range(numRooms):
    printList[i] = 'Room: ' + res[i][0] + ', ' + str(
        res[i][2]) + ' minute average visit, ' + str(
            res[i][1]) + ' visitor(s) total\n'

with open('new_csv.csv', "wb") as newFile:
    for line in printList:
        newFile.write(line)

# people_and_rooms = [(x[0], x[1]) for x in data]

# print(data)
