from flask import Flask

app = Flask(__name__)

@app.route('/')

def root():
    return( '''
            <h1> Hello Flask </h1>
            <hr/>
            <em> My First Flask Application! <em/>
            '''
            )


@app.route("/hello/<name>")
def hello(name):
    return( '''
            <h1> Hello Flask </h1>
            <hr/>
            <em> My First Flask Application! <em/>
            ''')

@app.route("/add/<int:number1>/<int:number2>")
def add(number1, number2):
    the_sum = number1 + number2
    return(f'''
            <h1>{number1} + {number2} = {the_sum}</h1>
            ''')
